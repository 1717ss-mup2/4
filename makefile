matrix: matrixMain.c
	gcc -std=c11 matrixMain.c matrix.c matrix.h -o matrix.out

run:
	./matrix.out
