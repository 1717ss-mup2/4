/*
 * File:   cramer.c
 * Author: Leonard Krause <kontakt@herr-ek.de>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"

int main(int argc, char** argv) {
  Matrix a = newMatrix(2,3);
  Matrix b = newMatrix(3,3);

  double aTemp[2][3] ={{4,2,1},{0,-2,4}};
  double bTemp[3][3] = {{1,2,3},{0,4,6},{2,-1,8}};
  for (int i=0; i<3; i++){
    for (int k=0; k<3; k++){
      (i<2) ? a[i][k]=aTemp[i][k] : i;
      b[i][k]=bTemp[i][k];
    }
  }
  //teste multiplikation
  printf("\n\nA\n");
  printMatrix(a,2,3);
  printf("\n\nB\n");
  printMatrix(b,3,3);
  printf("\n\nAxB\n");
  Matrix r = multMatrix(a,b,2,3,3);
  printMatrix(r,2,3);
  //teste scaling
  printf("\n\n1.5*AxB\n");
  scaleMatrix(r,2,3,1.5);
  printMatrix(r,2,3);
  //teste equals
  printf("\n\nA==1.5*(AxB)? ");
  printf(equalMatrix(a,r,2,3) ? "true" : "false");
  printf("\n\nA==A? ");
  printf(equalMatrix(a,a,2,3) ? "true" : "false");
  printf("\ndone\n");
  return (EXIT_SUCCESS);
}
