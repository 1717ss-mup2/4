/*
 * File:   cramer.c
 * Author: Leonard Krause <kontakt@herr-ek.de>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <regex.h>
#include <string.h>

/*
 *
 */

 #define RAND(n) -n + (double)rand()/RAND_MAX*2*n

typedef struct { double x; double y;} Punkt;
double norm( Punkt const * const p );
Punkt* randomPunkte( size_t len );
void printPunkte( Punkt const * const pts, size_t len );
int comparePunkte(const void* p1, const void* p2);

int main(int argc, char const *argv[]){
  size_t n = 10;
  if (argc==2){ //validate number of arguments
    char nString[strlen(argv[1])];
    for (size_t i=0; i<strlen(argv[1]); i++){
      nString[i] = argv[1][i];
    }
    regex_t regex;
    int regexR = regcomp(&regex, "^([0-9])+$", REG_EXTENDED);
    if (regexR) return (EXIT_FAILURE); //shouldnt happen
    regexR = regexec(&regex, &nString[0], 0, NULL, 0);//validate line
    if (!regexR){ //validate data of arguments
        sscanf(argv[1], "%lu",&n);
        if (n==0){
          printf("invalid argument (should be >0), using default value (10)\n");
          n=10;
        }
    }else{
      printf("invalid argument (should be a positive integer), using default value (10)\n");
    }
  }else{
    printf("invalid number of arguments, using default value (10)\n");
  }

  srand(time(NULL));
  Punkt* punkte = randomPunkte(n);
  printf("\n\nunsortiert:\n");
  printPunkte(punkte,n);
  qsort(punkte,n,sizeof(Punkt),comparePunkte);
  printf("\n\nsortiert:\n");
  printPunkte(punkte,n);
  printf("\ndone\n");
  return 0;
}

double norm( Punkt const * const p ){
   return sqrt(p->x * p->x + p->y * p->y);
}

Punkt* randomPunkte( size_t len ){
  Punkt* punkte = (Punkt*)malloc(sizeof(Punkt)*len);
  for (size_t i=0; i<=len-1; i++){
    punkte[i].x = RAND(100);
    punkte[i].y = RAND(100);
  }
  return punkte;
}

void printPunkte( Punkt const * const pts, size_t len ){
  for (size_t i=0; i<=len-1; i++){
    printf("(%.3g, %.3g) => %.3g | ",pts[i].x,pts[i].y,norm(&pts[i]));
  }
}

int comparePunkte(const void* p1, const void* p2){
  if(norm((Punkt*)p1) < norm((Punkt*)p2)){
    return -1;
  }else if(norm((Punkt*)p1) < norm((Punkt*)p2)){
    return 1;
  }
  return 0;
}
